//
//  LOGINViewController.swift
//  finalproject
//
//  Created by IFTS40 on 30/08/23.
//

import UIKit
import Firebase
import FirebaseDatabase
import CodableFirebase
import FirebaseAuth

class LOGINViewController: UIViewController {
    
    @IBOutlet weak var errorlbl: UILabel!
    @IBOutlet weak var emailtxt: UITextField!
    @IBOutlet weak var passwordtxt: UITextField!
    @IBOutlet weak var loginbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        
    }
    func setUpElements() {
        
        // Hide the error label
        errorlbl.alpha = 0
        
        // Style the elements
        Utilities.styleTextField(emailtxt)
        Utilities.styleTextField(passwordtxt)
        Utilities.styleFilledButton(loginbtn)
        
        
    }
    @IBAction func logintapped(_ sender: Any) {
        // TODO: Validate Text Fields
        
        // Create cleaned versions of the text field
        let email = emailtxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordtxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        // Signing in the user
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let authresult = result {
                let uid = authresult.user.uid // This is the unique Firebase UID
                
                
                if error != nil {
                    // Couldn't sign in
                    self.errorlbl.text = error!.localizedDescription
                    self.errorlbl.alpha = 1
                }   else {
                    
                    let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeViewController) as? menuTableViewController
                    
                    self.view.window?.rootViewController = homeViewController
                    self.view.window?.makeKeyAndVisible()
                }
            }
            
        }
    }
    
}

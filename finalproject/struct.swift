//
//  struct.swift
//  ggg
//
//  Created by IFTS40 on 23/08/23.

import Foundation
import CodableFirebase

struct User: Codable {
    var key : String = ""
    var nome: String
    var cognome: String
    var genere: String
    var cittadinascita: String
    var datadinascita: String?
    var codicefiscale: String
    var cellulare: String?
    var indirizzoemail: String
    var viadiresidenza: String
    var civicodiresidenza: String?
    var capdiresidenza: String?
    var cittadiresidenza: String
    var provinciadiresidenza: String
    var nazionediaresidenza: String
    static var uidToKeyMapping: [String: String] = [:]

    
    enum CodingKeys: String, CodingKey {
        case  key, nome, cognome, genere, cittadinascita, datadinascita, codicefiscale, cellulare, indirizzoemail, viadiresidenza, civicodiresidenza, capdiresidenza, cittadiresidenza, provinciadiresidenza, nazionediaresidenza
    }
    
    init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
        print(container)
        
       
            key = try container.decodeIfPresent(String.self, forKey: .key) ?? ""
            nome = try container.decode(String.self, forKey: .nome)
            cognome = try container.decode(String.self, forKey: .cognome)
            genere = try container.decode(String.self, forKey: .genere)
            cittadinascita = try container.decode(String.self, forKey: .cittadinascita)
            
            if let intDatadinascita = try? container.decode(Int.self, forKey: .datadinascita) {
                datadinascita = String(intDatadinascita)
            } else {
                datadinascita = try? container.decode(String.self, forKey: .datadinascita)
            }
            
            codicefiscale = try container.decode(String.self, forKey: .codicefiscale)
            
            if let intCellulare = try? container.decode(Int.self, forKey: .cellulare) {
                cellulare = String(intCellulare)
            } else {
                cellulare = try? container.decode(String.self, forKey: .cellulare)
            }
            
            // Decode other properties that can be both Int and String
            indirizzoemail = try container.decode(String.self, forKey: .indirizzoemail)
            viadiresidenza = try container.decode(String.self, forKey: .viadiresidenza)
            civicodiresidenza = try? container.decode(String.self, forKey: .civicodiresidenza)
            if let intcicvcodiresidenza = try? container.decode(Int.self, forKey: .civicodiresidenza) {
                civicodiresidenza = String(intcicvcodiresidenza)
            } else {
                civicodiresidenza = try? container.decode(String.self, forKey: .civicodiresidenza)
            }
            
            if let intcapdiresidenza = try? container.decode(Int.self, forKey: .capdiresidenza) {
                capdiresidenza = String(intcapdiresidenza)
            } else {
                capdiresidenza = try? container.decode(String.self, forKey: .capdiresidenza)
            }
            cittadiresidenza = try container.decode(String.self, forKey: .cittadiresidenza)
            provinciadiresidenza = try container.decode(String.self, forKey: .provinciadiresidenza)
            nazionediaresidenza = try container.decode(String.self, forKey: .nazionediaresidenza)
            
            
            
            
        }
        
        
        
        // Rest of the struct including encode(to:) and other methods...
        
        
        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            try container.encode(nome, forKey: .nome)
            try container.encode(cognome, forKey: .cognome)
            try container.encode(genere, forKey: .genere)
            try container.encode(cittadinascita, forKey: .cittadinascita)
            try container.encode(Int(datadinascita!) , forKey: .datadinascita)
            try container.encode(codicefiscale, forKey: .codicefiscale)
            try container.encode(Int(cellulare!) ?? 0, forKey: .cellulare)
            try container.encode(indirizzoemail, forKey: .indirizzoemail)
            try container.encode(viadiresidenza, forKey: .viadiresidenza)
            try container.encode(Int(civicodiresidenza!) ?? 0, forKey: .civicodiresidenza)
            try container.encode(Int(capdiresidenza!) ?? 0, forKey: .capdiresidenza)
            try container.encode(cittadiresidenza, forKey: .cittadiresidenza)
            try container.encode(provinciadiresidenza, forKey: .provinciadiresidenza)
            try container.encode(nazionediaresidenza, forKey: .nazionediaresidenza)
            
        }
        
    init(key: String, nome: String, cognome: String, genere: String, cittadinascita: String, datadinascita: String?, codicefiscale: String, cellulare: String?, indirizzoemail: String, viadiresidenza: String, civicodiresidenza: String?, capdiresidenza: String?, cittadiresidenza: String, provinciadiresidenza: String, nazionediaresidenza: String) {
        
        self.nome = nome
        self.cognome = cognome
        self.genere = genere
        self.cittadinascita = cittadinascita
        self.datadinascita = datadinascita
        self.codicefiscale = codicefiscale
        self.cellulare = cellulare
        self.indirizzoemail = indirizzoemail
        self.viadiresidenza = viadiresidenza
        self.civicodiresidenza = civicodiresidenza
        self.capdiresidenza = capdiresidenza
        self.cittadiresidenza = cittadiresidenza
        self.provinciadiresidenza = provinciadiresidenza
        self.nazionediaresidenza = nazionediaresidenza
        self.key = key
    }
    }
    


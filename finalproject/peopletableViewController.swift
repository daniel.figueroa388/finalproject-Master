//
//  peopletableViewController.swift
//  finalproject
//
//  Created by IFTS40 on 30/08/23.
//

import UIKit
import Firebase
import CodableFirebase
import FirebaseDatabase
var idUtente = ""
class peopletableViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    var users: [User] = []
    @IBOutlet weak var usertable: UITableView!
    private var items: [User] = [] {
        didSet {
            usertable.reloadData()
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFirebase()
        setupTableView()
        
    }


    
    func setupFirebase() {
        let databaseURL = "https://finalproject-70b3d-default-rtdb.firebaseio.com/"
        let ref = Database.database(url: databaseURL).reference().child("User")
      
        
        ref.observe(.childAdded) { snapshot in
            guard let value = snapshot.value else { return }
            //sprint(snapshot.value)
           //s print(value)
            
            
            do {
                let model = try FirebaseDecoder().decode(User.self, from: value)
                self.items.append(model)
                //sprint(model)
            } catch let error {
                print("errore",error)
            }
        } withCancel: { error in
            print("Firebase error:", error.localizedDescription)
        }
    }
    
    
    
    func setupTableView() {
        usertable.dataSource = self
        usertable.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cela12", for: indexPath) as! usserTableViewCell
        
        let item = items[indexPath.row]
        cell.nome.text = item.nome
        cell.cognome.text = item.cognome
        return cell
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return.delete
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (_, _, completionHandler) in
            tableView.beginUpdates()
            self.items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
        
        let updateAction = UIContextualAction(style: .normal, title: "Update") { [weak self] (_, _, completionHandler) in
            guard let self = self else { return }
            
            // Perform update operation here, e.g., navigate to another view controller
            idUtente = items[indexPath.row].key
            print("id",idUtente)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let updateViewController = storyboard.instantiateViewController(withIdentifier: "updateViewControllerIdentifier") as? updateViewController {
                // Pass any necessary data to the UpdateViewController if needed
                // updateViewController.data = self.items[indexPath.row]
                
                self.navigationController?.pushViewController(updateViewController, animated: true)
            }
            
            completionHandler(true)
        }
        
        // Customize the appearance of the update action (optional)
        updateAction.backgroundColor = .blue
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, updateAction])
        return configuration
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedUser = items[indexPath.row] // Assuming 'items' contains user data
        performSegue(withIdentifier: "details", sender: selectedUser)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "details" {
            if let destinationVC = segue.destination as? detailsViewController,
               let selectedUser = sender as? User { // Replace 'User' with your data model
                destinationVC.user = selectedUser
                
                
                
                
                
                
                
                
                
            }
        }
    }
}

 

//
//  detailsViewController.swift
//  finalproject
//
//  Created by IFTS40 on 30/08/23.
//

import UIKit

class detailsViewController: UIViewController {
    
    @IBOutlet weak var cap: UITextField!
    @IBOutlet weak var nometxt: UITextField!
    @IBOutlet weak var cognometxt: UITextField!
    @IBOutlet weak var generetxt: UITextField!
    @IBOutlet weak var civicotxt: UITextField!
    @IBOutlet weak var emailtxt: UITextField!
    @IBOutlet weak var birthdaytxt: UITextField!
    @IBOutlet weak var cellulkaretxt: UITextField!
    @IBOutlet weak var nazionediresidenzatxt: UITextField!
    @IBOutlet weak var viadiresidenza: UITextField!
    @IBOutlet weak var codicefisclae: UITextField!
    @IBOutlet weak var cittaditxt: UITextField!
    @IBOutlet weak var cittadiresidenzatxt: UITextField!
    @IBOutlet weak var provinciatxt: UITextField!
    var user : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.styleTextField(nometxt)
        Utilities.styleTextField(cognometxt)
        Utilities.styleTextField(generetxt)
        Utilities.styleTextField(civicotxt)
        Utilities.styleTextField(emailtxt)
        Utilities.styleTextField(birthdaytxt)
        Utilities.styleTextField(cellulkaretxt)
        Utilities.styleTextField(nazionediresidenzatxt)
        Utilities.styleTextField(viadiresidenza)
        Utilities.styleTextField(codicefisclae)
        Utilities.styleTextField(cittaditxt)
        Utilities.styleTextField(cittadiresidenzatxt)
        Utilities.styleTextField(provinciatxt)
        Utilities.styleTextField(cap)
        
        if let reciveddata = user{
            nometxt.text = user?.nome
            cognometxt.text = user?.cognome
            generetxt.text = user?.genere
            emailtxt.text = user?.indirizzoemail
            if let cellulareValue = user?.cellulare {
                let cellulareString = String(cellulareValue)
                cellulkaretxt.text = cellulareString
            } else {
                cellulkaretxt.text = nil // Handle the case when cellulareValue is nil
            }
            codicefisclae.text = user?.codicefiscale
            
            if let  birthda = user?.datadinascita {
                let birthstring = String(birthda)
                birthdaytxt.text = birthstring
            }else{
                birthdaytxt.text = nil
            }
            provinciatxt.text = user?.provinciadiresidenza
            nazionediresidenzatxt.text = user?.nazionediaresidenza
            viadiresidenza.text = user?.viadiresidenza
            if let civicodiresidenzaValue = user?.civicodiresidenza {
                let civicodiresidenzaString = String(civicodiresidenzaValue)
              civicotxt.text =  civicodiresidenzaString
            } else {
                civicotxt.text = nil
            }
            if let capval = user?.capdiresidenza{
                let capstring = String(capval)
                cap.text = capstring
            }else{
                cap.text = nil
            }
            cittadiresidenzatxt.text = user?.cittadiresidenza
            
           
           
                    
                }
        cittaditxt.text = user?.cittadinascita
                
            }
            
            
            
            
        }
        
    


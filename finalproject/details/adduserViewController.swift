//
//  adduserViewController.swift
//  finalproject
//
//  Created by IFTS40 on 31/08/23.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import CodableFirebase


class adduserViewController: UIViewController {
    
    @IBOutlet weak var nometxt: UITextField!
    @IBOutlet weak var cognometxt: UITextField!
    @IBOutlet weak var datadinscitatxt: UITextField!
    @IBOutlet weak var generetxt: UITextField!
    @IBOutlet weak var civicodiresidenzatxt: UITextField!
    @IBOutlet weak var indirizzioemailtxt: UITextField!
    @IBOutlet weak var cellularetxt: UITextField!
    @IBOutlet weak var viadiresidenzatxt: UITextField!
    @IBOutlet weak var captxt: UITextField!
    @IBOutlet weak var provinciadiresidenzatxt: UITextField!
    @IBOutlet weak var cittadinascitatxt: UITextField!
    @IBOutlet weak var codicefiscaletxt: UITextField!
    @IBOutlet weak var nazionediresidenzatxt: UITextField!
    @IBOutlet weak var cittadiresidenzatxt: UITextField!
    @IBOutlet weak var adduserbtn: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.styleTextField(nometxt)
        Utilities.styleTextField(cognometxt)
        Utilities.styleTextField(datadinscitatxt)
        Utilities.styleTextField(generetxt)
        Utilities.styleTextField(civicodiresidenzatxt)
        Utilities.styleTextField(indirizzioemailtxt)
        Utilities.styleTextField(cellularetxt)
        Utilities.styleTextField(viadiresidenzatxt)
        Utilities.styleTextField(captxt)
        Utilities.styleTextField(provinciadiresidenzatxt)
        Utilities.styleTextField(cittadinascitatxt)
        Utilities.styleTextField(codicefiscaletxt)
        Utilities.styleTextField(nazionediresidenzatxt)
        Utilities.styleTextField(cittadinascitatxt)
        Utilities.styleFilledButton(adduserbtn)
        Utilities.styleTextField(cittadiresidenzatxt)
    }
    
    @IBAction func addbtn(_ sender: Any) {
        guard let nome = nometxt.text ,
              let cognome = cognometxt.text ,
              let datadinascita = datadinscitatxt.text ,
              let genere = generetxt.text ,
              let civicodiresidenza = cittadiresidenzatxt.text ,
              let indrizzioemail = indirizzioemailtxt.text ,
              let celluleare = cellularetxt.text ,
              let viadiresidenza = viadiresidenzatxt.text ,
              let cap = captxt.text,
              let provincia  = provinciadiresidenzatxt.text ,
              let cittadinasci=cittadinascitatxt.text,
              let codicefis = codicefiscaletxt.text ,
              let nazionediresidenza = nazionediresidenzatxt.text,
              let cittadiresidenza = cittadiresidenzatxt.text else{
            return
        }
        var ref : DatabaseReference!
        
        
        ref = Database.database(url:"https://finalproject-70b3d-default-rtdb.firebaseio.com/").reference().child("User").childByAutoId()
        
        ref.child("nome").setValue(nome)
        ref.child("cognome").setValue(cognome)
        ref.child("genere").setValue(genere)
        ref.child("civicodiresidenza").setValue(civicodiresidenza)
        ref.child("indirizzoemail").setValue(indrizzioemail)
        ref.child("cellulare").setValue(celluleare)
        ref.child("viadiresidenza").setValue(viadiresidenza)
        ref.child("capdiresidenza").setValue(cap)
        ref.child( "provinciadiresidenza").setValue(provincia)
        ref.child("cittadinascita").setValue(cittadinasci)
        ref.child("codicefiscale").setValue(codicefis)
        ref.child("nazionediaresidenza").setValue(nazionediresidenza)
        ref.child("cittadiresidenza").setValue(cittadiresidenza)
        ref.child("datadinascita").setValue(datadinascita)
    }
 
    
    
}


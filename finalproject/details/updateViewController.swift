//
//  updateViewController.swift
//  finalproject
//
//  Created by IFTS40 on 31/08/23.
//

import UIKit
import Firebase

class updateViewController: UIViewController {
    
    var userID: String = "uid" // Initialize with the correct user ID
    
    
    @IBOutlet weak var newnome: UITextField!
    @IBOutlet weak var newcognomw: UITextField!
    @IBOutlet weak var dtadinascita: UITextField!
    @IBOutlet weak var genrenew: UITextField!
    @IBOutlet weak var civicodiresidenzzanew: UITextField!
    @IBOutlet weak var emailnew: UITextField!
    @IBOutlet weak var newcellulare: UITextField!
    @IBOutlet weak var newcapdiresdenza: UITextField!
    @IBOutlet weak var cittadiresidenzanew: UITextField!
    @IBOutlet weak var codicifisnew: UITextField!
    @IBOutlet weak var cittadinascitanew: UITextField!
    @IBOutlet weak var nazinenew: UITextField!
    @IBOutlet weak var provinciadinascitanew: UITextField!
    @IBOutlet weak var viadiresidenzanew: UITextField!
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Call the function to fetch user data
        fetchUserData()
        updateui()
    }
    func updateui(){
        guard let user = user else {return}
        newnome.text = user.nome
        newcognomw.text = user.cognome
        genrenew.text = user.genere
        civicodiresidenzzanew.text = user.civicodiresidenza
        emailnew.text = user.indirizzoemail
        newcellulare.text = user.cellulare
        viadiresidenzanew.text = user.viadiresidenza
        newcapdiresdenza.text = user.capdiresidenza
        provinciadinascitanew.text = user.provinciadiresidenza
        cittadinascitanew.text = user.cittadinascita
        codicifisnew.text = user.codicefiscale
        nazinenew.text = user.nazionediaresidenza
        cittadiresidenzanew.text = user.cittadinascita
        dtadinascita.text = user.datadinascita
    }

    
    func fetchUserData() {
        let databaseURL = "https://finalproject-70b3d-default-rtdb.firebaseio.com/"
        
        let userReference = Database.database(url: databaseURL).reference().child("User").child(userID)
        userReference.observeSingleEvent(of: .value) { [weak self] snapshot, error in
            guard let self = self else { return }
            if let error = error {
                // Handle error
                print("Error: \(error)")
            } else {
                if snapshot.exists() {
                    // Process snapshot data
                    if let userData = snapshot.value as? [String: Any] {
                        // Call the function to update UI with fetched data
                        self.updateUI(with: userData)
                    }
                } else {
                    // Handle the case when the snapshot doesn't exist
                    print("Snapshot doesn't exist")
                }
            }
        }
    }
    
    
    func updateUI(with userData: [String: Any]) {
        if let nome = userData["nome"] as? String,
           let cognome = userData["cognome"] as? String,
           let genere = userData["genere"] as? String,
           let civicodiresidenza = userData["civicodiresidenza"] as? String,
           let indirizzoemail = userData["indirizzoemail"] as? String,
           let cellulare = userData["cellulare"] as? String,
           let viadiresidenza = userData["viadiresidenza"] as? String,
           let capdiresidenza = userData["capdiresidenza"] as? String,
           let provinciadiresidenza = userData["provinciadiresidenza"] as? String,
           let cittadinascita = userData["cittadinascita"] as? String,
           let codicefiscale = userData["codicefiscale"] as? String,
           let nazionediaresidenza = userData["nazionediaresidenza"] as? String,
           let cittadiresidenza = userData["cittadiresidenza"] as? String,
           let datadinascita = userData["datadinascita"] as? String {
            
        }
    }
        
        @IBAction func updateuser(_ sender: Any) {
            // Create a dictionary with updated data
            let userData: [String: Any] = [
                "nome": newnome.text ?? "",
                "cognome": newcognomw.text ?? "",
                // Add other fields...
            ]
            
            // Assuming you have a reference to the database URL
            let databaseURL = "https://finalproject-70b3d-default-rtdb.firebaseio.com/"
            let databaseRef = Database.database(url: databaseURL).reference().child("User").child(idUtente)
            
            // Update the user's data in the database
            databaseRef.updateChildValues(userData) { [weak self] error, _ in
                if let error = error {
                    print("Error updating user data: \(error)")
                } else {
                    print("User data updated successfully in the database")
                }
            }
        }
    
    }
